import java.util.Date;

public class Book {

	private String author;

	private String name;

	private Integer price;

	private Date publishDate;

	public String getAuthor() {
		return author;
	}

	public String getName() {
		return name;
	}

	public Integer getPrice() {
		return price;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setAuthor(final String author) {
		this.author = author;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setPrice(final Integer price) {
		this.price = price;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	@Override
	public String toString() {
		return name;
	}

}