package beans;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Test {

	public static void main(String[] args)

	{

		// Creating 1st Object instance of Book
		Book b1 = new Book();
		b1.setAuthor("Varghese");
		b1.setName("JAVA 101");
		b1.setPrice(10);
		b1.setPublishDate(new Date());
		/*
		 * System.out.println("................. Start"); System.out.println(b1.getName());
		 * System.out.println(b1.getAuthor()); System.out.println(b1.getPrice());
		 * System.out.println(b1.getPublishDate());
		 */
		// Creating 2nd instance of the Book
		Book b2 = new Book();
		b2.setAuthor("Adam");
		b2.setName("Java Programming");
		b2.setPrice(100);
		b2.setPublishDate(new Date());
		/*
		 * System.out.println(".................  Start \n"); System.out.println(b2.getName());
		 * System.out.println(b2.getAuthor()); System.out.println(b2.getPrice());
		 * System.out.println(b2.getPublishDate());
		 */
		// 3rd instance of Book
		Book b3 = new Book();
		b3.setAuthor("Author_3");
		b3.setName("Book_name_3");
		b3.setPrice(33);
		b3.setPublishDate(new Date());
		/*
		 * System.out.println("................... Start\n"); System.out.println(b3.getName());
		 * System.out.println(b3.getAuthor()); System.out.println(b3.getPrice());
		 * System.out.println(b3.getPublishDate());
		 */

		// 4th instance of Book
		Book b4 = new Book();
		b4.setAuthor("Tester_4");
		b4.setName("Test_Name_4");
		b4.setPrice(15);
		b4.setPublishDate(new Date());
		/*
		 * System.out.println("..................... Start\n"); System.out.println(b4.getName());
		 * System.out.println(b4.getAuthor()); System.out.println(b4.getPrice());
		 * System.out.println(b4.getPublishDate());
		 */

		// Generic List
		List<Book> books = new ArrayList<Book>();
		books.add(b1);
		books.add(b2);
		books.add(b3);
		books.add(b4);

		int size = books.size();
		System.out.println(size + " is the Total number of objects in the Generic List");

		// Iterator<Book> iter = books.iterator();
		// while (iter.hasNext()) {
		// iter.hasNext();
		// System.out.println("a");
		// iter.hasNext();
		// System.out.println("b");
		//
		// Book retr = iter.next();
		//
		// System.out.println("\n Start.........888..........." + retr);
		// System.out.println(retr.getAuthor());
		// System.out.println(retr.getName());
		// System.out.println(retr.getPublishDate());
		// System.out.println(retr.getPrice());
		// System.out.println("End.....................");
		// }

		for (Book book : books) {
			System.out.println("\n Start...................");
			System.out.println(book.getAuthor());
			System.out.println(book.getName());
			System.out.println(book.getPublishDate());
			System.out.println(book.getPrice());
			System.out.println("End.....................");
		}

		/*
		 * for (Iterator<Book> iter = books.iterator(); iter.hasNext();) { Book s = iter.next(); System.out.print(s); }
		 */

		for (int i = 0; i < size; i++) {

			Book book = books.get(i);

			System.out.println("\n Start.................... Book Number:" + i);
			System.out.println(book.getAuthor());
			System.out.println(book.getName());
			System.out.println(book.getPublishDate());
			System.out.println(book.getPrice());
			System.out.println("End.....................");
		}

		for (int i = 0; i < size; i++) {
			System.out.println("TEST........ \n");
			System.out.println(books.get(i).getName());
		}

		//
		// Never use AWT
		// Never to declare an array this way
		// List boooooox = new ArrayList();
		// boooooox.add(b1);
		// boooooox.add("JOE");

	}
}
